package fr.uavignon.ceri.tp3.data.database;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.tp3.data.Objet;

@Database(entities = {Objet.class}, version = 4, exportSchema = false)
public abstract class ObjetRoomDatabase extends RoomDatabase {

    private static final String TAG = ObjetRoomDatabase.class.getSimpleName();

    public abstract ObjetDao objetDao();

    private static ObjetRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static ObjetRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ObjetRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    // without populate

                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    ObjetRoomDatabase.class,"objet_database")
                                    .fallbackToDestructiveMigration()
                                    .build();




                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        ObjetDao dao = INSTANCE.objetDao();

                        //dao.deleteAll();
                    });

                }
            };



}


