package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

public class DetailFragment extends Fragment {///
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView humidity;
    private TextView objetName;
    private ProgressBar progress;
    private TextView cloudiness;
    private TextView wind;
    private TextView description;
    private TextView temperature;
    private TextView lastUpdate;
    private ImageView imgWeather;



    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected city
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String objetID = args.getItemNum();
        Log.d(TAG,"selected id="+ objetID);
        viewModel.setObjet(objetID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {

        humidity = getView().findViewById(R.id.editHumidity);
        //cloudiness = getView().findViewById(R.id.editCloudiness);
        wind = getView().findViewById(R.id.editWind);
        temperature = getView().findViewById(R.id.editTemperature);
        //lastUpdate = getView().findViewById(R.id.editLastUpdate);
        objetName = getView().findViewById(R.id.nameCity);
        description = getView().findViewById(R.id.country);
        imgWeather = getView().findViewById(R.id.iconeWeather);

        progress = getView().findViewById(R.id.progress);



        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {


        viewModel.getObjet().observe(getViewLifecycleOwner(),
                objet -> {
                    if (objet != null) {
                        Log.d(TAG, "observ  view objet");

                        objetName.setText(objet.getName());
                        description.setText(objet.getDescription());

                        if(objet.getWorking()==true){
                            temperature.setText("Objet fonctionnel");
                        }
                        else{
                            temperature.setText("Objet non fonctionnel");
                        }
                        if(objet.getYear()==-1 || objet.getYear()==0){
                            humidity.setText("Inconnu");
                        }
                        else{
                            humidity.setText(String.valueOf(objet.getYear()));
                        }
                        wind.setText(objet.getBrand());

                    }
                });

        viewModel.getIsLoading().observe(getViewLifecycleOwner(),
                isLoading ->{
                    if(isLoading){

                        progress.setVisibility(View.VISIBLE);
                    }
                    else{
                        progress.setVisibility(View.GONE);
                    }
                }
        );
        viewModel.getWebServiceThrowable().observe(getViewLifecycleOwner(),
                throwable ->{

                    Snackbar snackbar = Snackbar
                            .make(getView(), throwable.getMessage(), Snackbar.LENGTH_LONG);

                    snackbar.show();
                }
        );
    }


}
