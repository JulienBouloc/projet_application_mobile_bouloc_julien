package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;


import fr.uavignon.ceri.tp3.data.database.ObjetDao;
import fr.uavignon.ceri.tp3.data.database.ObjetRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.CeriInterface;
import fr.uavignon.ceri.tp3.data.webservice.ObjResponse;
import fr.uavignon.ceri.tp3.data.webservice.ObjResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase.databaseWriteExecutor;

public class CeriRepository {

    private static final String TAG = CeriRepository.class.getSimpleName();

    private LiveData<List<City>> allCities;
    private MutableLiveData<Objet> selectedObjet =new MutableLiveData<>();;

    private final CeriInterface api;

    public MutableLiveData<Boolean> isLoading=new MutableLiveData<Boolean>();
    public MutableLiveData<Throwable> webServiceThrowable=new MutableLiveData<>();

    private ObjetDao objetDao;


    volatile int nbAPILoads=0;


    private MutableLiveData<ArrayList<Objet>> objets =new MutableLiveData<>();

    public MutableLiveData<ArrayList<Objet>> getObjets() {
        return objets;
    }

    private static volatile CeriRepository INSTANCE;

    public synchronized static CeriRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new CeriRepository(application);
        }

        return INSTANCE;
    }

    public CeriRepository(Application application) {
        ObjetRoomDatabase db = ObjetRoomDatabase.getDatabase(application);
        objetDao = db.objetDao();
        // = objetDao.getAllCities();
        //selectedCity = new MutableLiveData<>();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(CeriInterface.class);
    }




    public MutableLiveData<Objet> getSelectedObjet() {
        return selectedObjet;
    }

    public void loadWeatherAllCities(){
        //List<City> cities= objetDao.getSynchrAllCities();
        //nbAPILoads=cities.size();
        /*for (int i=0;i<cities.size();i++){
            loadCollection();
        }*/
        //nbAPILoads=allCities.getValue().size();
        /*
        for (int i=0;i<allCities.getValue().size();i++){
            loadWeatherCity(allCities.getValue().get(i));
        }

         */
    }


    public void removeAll(){
        System.out.println("SUPP");
        objetDao.deleteAll();
        objets.postValue(null);
    }


    public void loadCollectionFromDatabase(){
        ArrayList<Objet> allObjets = (ArrayList<Objet>) objetDao.getSynchrAllObjets();
        objets.postValue(allObjets);
    }

    public void getObjetsByYear(){
        ArrayList<Objet> allObjets = (ArrayList<Objet>) objetDao.getObjetsByYear();
        objets.postValue(allObjets);
    }

    public void getObjetsByName(){
        ArrayList<Objet> allObjets = (ArrayList<Objet>) objetDao.getObjetsByName();
        objets.postValue(allObjets);
    }

    public void loadCollection(){
        isLoading.postValue(Boolean.TRUE);




        api.getCollection().enqueue(
                new Callback<Map<String, ObjResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, ObjResponse>> call,
                                           Response<Map<String, ObjResponse>> response) {
                        //Log.d("API",response.body().toString());
                        ArrayList<Objet> objetTMP =new ArrayList<>();
                        int i=0;
                        for (String key: response.body().keySet()) {
                            System.out.println(key + "=" + response.body().get(key).name);
                            //System.out.println(response.body().get(key).working);
                            //objetTMP.add(new Objet(key,response.body().get(key).name,response.body().get(key).description));
                            ObjResult.transferInfo(response.body().get(key), key, objetTMP);
                            System.out.println(objetTMP.get(i).getWorking());
                            long res=insertObjet(objetTMP.get(i));
                            System.out.println("INSERTION:"+res);
                            i=i+1;


                        }

                        objets.setValue(objetTMP);
                        /*
                        if (nbAPILoads<=1){
                            isLoading.postValue(Boolean.FALSE);
                        }
                        else{
                            nbAPILoads=nbAPILoads-1;
                        }
                        */
                        isLoading.postValue(Boolean.FALSE);
                        /*
                        if (objets.getValue()!=null){
                            System.out.println("LISTE :");
                            for (int i=0;i<objets.getValue().size();i++){
                                System.out.println(objets.getValue().get(i).getName());
                            }
                        }
                        else{
                            System.out.println("List null");
                        }

                         */
                        // System.out.println(selectedObjet.getValue().getName());
                    }

                    @Override
                    public void onFailure(Call<Map<String, ObjResponse>> call, Throwable t) {
                        Log.d("FAILURE API",t.getMessage());
                        if (nbAPILoads<=1){
                            isLoading.postValue(Boolean.FALSE);
                        }
                        else{
                            nbAPILoads=nbAPILoads-1;
                        }
                        webServiceThrowable.postValue(t);
                    }
                });

    }


    public long insertObjet(Objet newObjet) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return objetDao.insert(newObjet);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //if (res != -1)
        //   selectedCity.setValue(newObjet);
        return res;
    }

    public int updateCity(Objet objet) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return objetDao.update(objet);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedObjet.setValue(objet);
        return res;
    }

    public void deleteCity(String id) {
        databaseWriteExecutor.execute(() -> {
            objetDao.deleteObjet(id);
        });
    }

    public void getObjet(String id)  {

        Future<Objet> futureobjet = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return objetDao.getObjetById(id);
        });
        try {
            selectedObjet.setValue(futureobjet.get());
            //System.out.println(futureobjet.get().getName());
            // System.out.println(selectedObjet.getValue().getName());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
