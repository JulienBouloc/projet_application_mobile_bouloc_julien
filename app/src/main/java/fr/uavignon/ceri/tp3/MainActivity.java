package fr.uavignon.ceri.tp3;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;



public class MainActivity extends AppCompatActivity {

    ListViewModel listViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listViewModel = new ViewModelProvider(this).get(ListViewModel.class);
        listViewModel.loadCollectionFromDatabase();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_update_all) {
            ///

            listViewModel.loadCollection();
            //View view=findViewById(android.R.id.content);

            System.out.println("terminé");

            return true;
        }
        if (id == R.id.action_remove_all) {
            ///

            listViewModel.removeAll();
            //View view=findViewById(android.R.id.content);

            System.out.println("terminé");

            return true;
        }

        if(id== R.id.tri_year){
            listViewModel.getObjetsByYear();
            return true;
        }

        if(id== R.id.tri_name){
            listViewModel.getObjetsByName();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

