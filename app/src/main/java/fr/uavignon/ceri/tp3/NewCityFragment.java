package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public class NewCityFragment extends Fragment {


    public static final String TAG = NewCityFragment.class.getSimpleName();

    private NewCityViewModel viewModel;
    private EditText editNewName, editNewCountry;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(NewCityViewModel.class);
        ///

    }


    private void listenerSetup() {
        editNewName = getView().findViewById(R.id.editNewName);
        editNewCountry = getView().findViewById(R.id.editNewCountry);

        getView().findViewById(R.id.buttonInsert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ///
                           }


        });

        getView().findViewById(R.id.buttonBack2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
            }
        });
    }

    private void observerSetup() {

        viewModel.getObjet().observe(getViewLifecycleOwner(),
                objet -> {
                    if (objet != null) {

                        Log.d(TAG, "observe view objet");

                        editNewName.setText(objet.getName());

                        editNewCountry.setText(objet.getDescription());
                    }
                });

    }
}
