package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.CeriRepository;
import fr.uavignon.ceri.tp3.data.Objet;


public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private CeriRepository repository;

    private  MutableLiveData<Throwable> webServiceThrowable;

    private MutableLiveData<Objet> objet;

    private MutableLiveData<Boolean> isLoading;


    public DetailViewModel (Application application) {///

        super(application);
        isLoading=new MutableLiveData<Boolean>();

        repository = CeriRepository.get(application);
        isLoading=repository.isLoading;

        webServiceThrowable=repository.webServiceThrowable;
        objet = new MutableLiveData<>();
    }



    public void setObjet(String id) {///
        repository.getObjet(id);

        objet = repository.getSelectedObjet();
        //
    }



    LiveData<Objet> getObjet() {
        return objet;
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<Throwable> getWebServiceThrowable(){return webServiceThrowable;}


}
