package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.CeriRepository;
import fr.uavignon.ceri.tp3.data.Objet;


public class ListViewModel extends AndroidViewModel {///
    private CeriRepository repository;
    private MutableLiveData<Boolean> isLoading;

    private MutableLiveData<ArrayList<Objet>> allObjet;

    public ListViewModel (Application application) {
        super(application);
        //repository.loadCollection();
        ArrayList<Objet> objets=new ArrayList<Objet>();

        allObjet =new MutableLiveData<>();

        repository = CeriRepository.get(application);

        isLoading=repository.isLoading;
        //
        allObjet =repository.getObjets();
    }


    public void deleteCity(String id) {
        repository.deleteCity(id);
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<ArrayList<Objet>> getAllObjet() {
        return allObjet;
    }

    public void loadCollection(){///
        //repository.loadWeatherAllCities();
        Thread t = new Thread(){
            public void run(){
                repository.loadCollection();

            }
        };

        t.start();

    }

    public void loadCollectionFromDatabase(){///
        //repository.loadWeatherAllCities();
        Thread t = new Thread(){
            public void run(){
                repository.loadCollectionFromDatabase();

            }
        };

        t.start();

    }

    public void getObjetsByYear(){///
        //repository.loadWeatherAllCities();
        Thread t = new Thread(){
            public void run(){
                repository.getObjetsByYear();

            }
        };

        t.start();

    }

    public void getObjetsByName(){///
        //repository.loadWeatherAllCities();
        Thread t = new Thread(){
            public void run(){
                repository.getObjetsByName();

            }
        };

        t.start();

    }

    public void removeAll(){///

        Thread t = new Thread(){
            public void run(){
                repository.removeAll();

            }
        };

        t.start();

    }

}

