package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.CeriRepository;
import fr.uavignon.ceri.tp3.data.Objet;


public class NewCityViewModel extends AndroidViewModel {

    public static final String TAG = DetailViewModel.class.getSimpleName();

    private CeriRepository repository;

    private MutableLiveData<Objet> objet;

    public NewCityViewModel (Application application) {

        super(application);

        repository = CeriRepository.get(application);

        objet = new MutableLiveData<>();
    }

    public void setCity(String id) {

        repository.getObjet(id);

        objet = repository.getSelectedObjet();
    }

    LiveData<Objet> getObjet() {
        return objet;
    }

    //remove public long insertOrUpdateCity(City newCity)
    ///

}

