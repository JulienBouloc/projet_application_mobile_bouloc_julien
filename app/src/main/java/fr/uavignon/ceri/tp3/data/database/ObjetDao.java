package fr.uavignon.ceri.tp3.data.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Objet;

@Dao
public interface ObjetDao {


        @Insert(onConflict = OnConflictStrategy.IGNORE)
        long insert(Objet objet);

        @Query("DELETE FROM objet_database")
        void deleteAll();

        @Query("SELECT * from objet_database ORDER BY name ASC")
        List<Objet> getSynchrAllObjets();

        @Query("SELECT * from objet_database ORDER BY year ASC")
        List<Objet> getObjetsByYear();

        @Query("SELECT * from objet_database ORDER BY name ASC")
        List<Objet> getObjetsByName();

        @Query("DELETE FROM objet_database WHERE _id = :id")
        void deleteObjet(String id);

        @Query("SELECT * FROM objet_database WHERE _id = :id")
        Objet getObjetById(String id);

        @Update(onConflict = OnConflictStrategy.IGNORE)
        int update(Objet objet);
}

